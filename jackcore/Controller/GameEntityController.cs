﻿using jackcore.View;

namespace jackcore.Controller
{
    public abstract class GameEntityController<T> where T : EntityComponent
    {
        private readonly T _component;
        public GameEntityController(EntityComponent parent)
        {
            _component = parent.GetComponentInChildren<T>();
        }
    }
}