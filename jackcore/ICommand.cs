﻿namespace jackcore
{
    public interface ICommand
    {
        void Execute();
    }
}