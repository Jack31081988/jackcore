﻿using System;

namespace jackcore.Model
{
    public class Observe<T>
    {
        public Action<T> OnChange;

        private T _value;

        public T Value
        {
            get { return _value; }
            set
            {
                if (!_value.Equals(value))
                {
                    if (OnChange != null) OnChange(value);
                }
                _value = value;
            }
        }
    }
}